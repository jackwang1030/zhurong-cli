#! /usr/bin/env node

const program = require('commander');
// const chalk = require('chalk')
const figlet = require('figlet');

program
  .command('create <app-name>')
  .description('create a new project')
  // -f or -force 为强制创建，如果创建目录存在则直接覆盖
  .option('-f, --force', 'overwrite target directory if it exists')
  .action((name, cmd) => {
    // console.log('create', name, 'cmd', cmd);
    // 在 create.js 中执行创建任务
    require('../lib/create.js')(name, cmd);
  });

program
  // 配置版本号信息
  .version(`v${require('../package.json').version}`)
  .usage('<command> [options]');

program
  .on('--help', () => {
    // 新增说明信息
    // console.log(`\r\nRun ${chalk.cyan('zr <command> --help')} for detailed usage of given command.\r\n`); 
    console.log('\r\n' + figlet.textSync('zhurong', { 
      font: 'Standard',
      horizontalLayout: 'default',
      verticalLayout: 'default',
      width: 80,
      whitespaceBreak: true 
    }));
  })

// 解析用户执行命令的参数
program.parse(process.argv);
