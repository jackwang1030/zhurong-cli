const path = require('path');
// fs-extra 是对 fs 模块的拓展，支持 promise 语法
const fs = require('fs-extra');
const inquirer = require('inquirer');
const Generator = require('./Generator')

module.exports = async function (name, options) {
    // 当前命令行选择的目录 
    const cwd = process.cwd();
    // 需要创建的目录地址
    const targetDir = path.resolve(cwd, name);
    // 目录是否存在
    if(fs.existsSync(targetDir)) {
        if(options.force) {
            await fs.remove(targetDir);
        } else {
           // throw new Error('target directory is already exists');
           // 询问用户是否覆盖
           let { action } = await inquirer.prompt([
            {
                name: 'action',
                type: 'list',
                message: `target directory is already exists, do you want to overwrite it?`,
                choices: [
                    {
                        name: 'overwrite',
                        value: 'overwrite'
                    },
                    {
                        name: 'Cancel',
                        value: false
                    }
                ]
            }
          ])
            if(!action) {
                return;
            }else if (action === 'overwrite') {
                console.log('\r\nRemoving...')
                await fs.remove(targetDir);
                console.log('\r\nRemoving Success!')
            }
            
        }
    }

    const generator = new Generator(name, targetDir);
    await generator.create();
}